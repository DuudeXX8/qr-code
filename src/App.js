import { Fragment, useEffect, useState } from "react";
import { Route, Routes, useLocation } from "react-router-dom";
import RevanVaccine from "./pages/Revan";
import VikaVaccine from "./pages/Vika";
import Spinner from "./components/spinner/Spinner";

const routes = {
  revan: "/covid-cert/status/62059699-fdd1-4d8c-bee7-203acf1ca941",
  vika: "/covid-cert/status/62059699-fdd1-4d8c-bee7-203acf1ca942",
};

function App() {
  const { pathname } = useLocation();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (pathname !== routes.revan && pathname !== routes.vika) {
      window.location.href = "https://www.gosuslugi.ru/";
    }
  }, [pathname]);

  useEffect(() => {
    if (loading) {
      setTimeout(() => {
        setLoading(false);
      }, 500);
    }
  }, [loading]);

  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        <Routes>
          <Route path={routes.revan} element={<RevanVaccine />} />
          <Route path={routes.vika} element={<VikaVaccine />} />
        </Routes>
      )}
    </Fragment>
  );
}

export default App;
