import { ReactComponent as GreenBG } from "./bg.svg";

const Certificate = ({ langRu, number }) => {
  return (
    <div className="status-container mt-28">
      <div id="green-bg" className="status-container-image">
        <GreenBG />
      </div>
      <div className="status-container-inner">
        <h4 id="main-title" className="title-h4 white status-title main-title">
          {langRu ? "COVID-19 certificate" : "Сертификат COVID-19"}
        </h4>
        <div className="status mt-12 text-plain small-text bold">
          <span id="status" className="status-value cert-name">
            {langRu ? "Valid" : "Действителен"}
          </span>
        </div>
        <h4 id="cert-id-block" className="title-h4 white status-title mt-12">
          <span className="num-symbol">№</span>{" "}
          <span className="unrz" id="cert-id">
            {number}{" "}
          </span>
        </h4>
      </div>
    </div>
  );
};

export default Certificate;
