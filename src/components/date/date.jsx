import { ReactComponent as DateIcon } from "./DateIcon.svg";

const DateShower = ({ langRu }) => {
  return (
    <div id="dates-block" className="person-data person-data-dates mt-12">
      <div className="mb-4 person-data-wrap align-items-center">
        <div className="person-date mr-12">
          <DateIcon />
        </div>
        <div id="dates" className="small-text gray mr-4">
          {langRu ? "Valid until" : "Действует до"} 10.12.2023
        </div>
      </div>
    </div>
  );
};

export default DateShower;
