import { ReactComponent as RussianFlag } from "./russianflag.svg";
import { ReactComponent as ENFlag } from "./enflag.svg";
import "./style.css";
import "./gosuslugi.min.css";
import { useState } from "react";
import Certificate from "./certificate/certificate";
import DateShower from "./date/date";
import Person from "./person/person";

const Main = ({ date, fullNameRu, fullNameEng, passport, number }) => {
  const [toggleRu, setToggleRu] = useState(false);

  const urlToMainPage = () => {
    window.location.href = "https://www.gosuslugi.ru/";
  };

  const toggleFlag = () => setToggleRu(!toggleRu);

  return (
    <>
      <div className="flex-container ml-6 mr-6 justify-between align-items-center mt-52 mb-32">
        <div className="ml-24">
          <span onClick={urlToMainPage} className="logo" />
        </div>
        <div
          id="translate-button"
          className="translate-button flex-container mt-6 mr-24 align-items-center"
        >
          <div className="mr-8">
            <div className="lang-image">
              <div className={`lang-${toggleRu ? "ru" : "en"}`}>
                {toggleRu ? (
                  <RussianFlag onClick={toggleFlag} />
                ) : (
                  <ENFlag onClick={toggleFlag} />
                )}
              </div>
            </div>
          </div>
          <div id="lang-label" className="lang">
            {toggleRu ? "ENG" : "RUS"}
          </div>
        </div>
      </div>
      <Certificate langRu={toggleRu} number={number} />
      <DateShower langRu={toggleRu} />
      <Person
        langRu={toggleRu}
        fullName={toggleRu ? fullNameEng : fullNameRu}
        date={date}
        passport={passport}
      />
      <div className="mt-24">
        <a id="close" href="/" className="button">
          {toggleRu ? "Close" : "Закрыть"}
        </a>
      </div>
    </>
  );
};

export default Main;
