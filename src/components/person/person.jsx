const Person = ({ langRu, date, fullName, passport }) => {
  return (
    <div className="person-data person-data-attrs mt-24">
      <div id="full-name-block" className="mb-4 person-data-wrap attr-wrap">
        <div id="full-name" className="attr-value title-h6 bold text-center">
          {fullName}
        </div>
      </div>
      <div id="other-attrs">
        <div className="mb-4 person-data-wrap attr-wrap">
          <div className="small-text mb-4 mr-4 attr-title">
            {langRu ? "Date of birth:" : "Дата рождения:"}{" "}
          </div>
          <div className="attr-value small-text gray">{date}</div>
        </div>
        <div className="mb-4 person-data-wrap attr-wrap">
          <div className="small-text mb-4 mr-4 attr-title">
            {langRu ? "National passport:" : "Паспорт:"}{" "}
          </div>
          <div className="attr-value small-text gray">{passport}</div>
        </div>
      </div>
    </div>
  );
};

export default Person;
