const Spinner = () => {
  return (
    <div className="container-app-loader" id="start-app-loader">
      <div className="container-pulse pulse animated"></div>
      <div className="container-app-logo">
        <div className="app-logo"></div>
      </div>
    </div>
  );
};

export default Spinner;
