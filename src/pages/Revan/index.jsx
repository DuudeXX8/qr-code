import Main from "../../components/main";

const RevanVaccine = () => {
  return (
    <Main
      date="25.09.1992"
      passport="C0****984"
      fullNameRu="Л****** Р**** А****"
      fullNameEng="L****** R**** А****"
      number="4046 2110 2210 9183"
    />
  );
};

export default RevanVaccine;
