import Main from "../../components/main";

const VikaVaccine = () => {
  return (
    <Main
      date="18.02.1997"
      passport="C0****177"
      fullNameRu="Г****** Е******* Д*******"
      fullNameEng="H****** E**** D****"
      number="4994 4210 8955 0925"
    />
  );
};

export default VikaVaccine;
